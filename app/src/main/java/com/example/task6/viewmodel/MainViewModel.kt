package com.example.task6.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.task6.adapter.AtmMachineAdapter
import com.example.task6.adapter.BankFilialAdapter
import com.example.task6.adapter.InfoboxAdapter
import com.example.task6.api.entity.DistanceComparator
import com.example.task6.repository.Repository
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.await
import java.net.UnknownHostException
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private val atmMachineCoordsLiveData = MutableLiveData<List<LatLng>>()
    val atmMachineCoords: LiveData<List<LatLng>>
        get() = atmMachineCoordsLiveData

    private val errorHandlerLiveData = MutableLiveData(false)
    val errorHandler: LiveData<Boolean>
        get() = errorHandlerLiveData

    private val bankRelatedPlaces = mutableListOf<LatLng>()

    fun findAllAtmCoordinates(cityName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                bankRelatedPlaces.addAll(
                    repository.getAllAtms(cityName).await()
                        .map { AtmMachineAdapter.instance.atmMachineToLatLng(it) })
                bankRelatedPlaces.addAll(
                    repository.getAllBankFilials(cityName).await()
                        .map { BankFilialAdapter.instance.bankFilialToLatLng(it) })
                bankRelatedPlaces.addAll(
                    repository.getAllInfoboxes(cityName).await()
                        .map { InfoboxAdapter.instance.infoboxToLatLng(it) })
                getNearestPlaces(bankRelatedPlaces)
            }catch (e:UnknownHostException){
                errorHandlerLiveData.postValue(true)
            }
        }
    }

    private fun getNearestPlaces(
        places: List<LatLng>, amount: Int = 10
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val distanceComparator = DistanceComparator()
            val nearestPlaces = places.sortedWith(distanceComparator).distinct().take(amount)
            atmMachineCoordsLiveData.postValue(nearestPlaces)
        }
    }
}