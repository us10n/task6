package com.example.task6.api.entity

import com.example.task6.utils.DistanceCounter
import com.google.android.gms.maps.model.LatLng

class DistanceComparator : Comparator<LatLng> {
    override fun compare(p0: LatLng, p1: LatLng): Int {
        val gomel = LatLng(52.425163, 31.015039)
        return DistanceCounter.distanceBetween(p0, gomel)
            .compareTo(DistanceCounter.distanceBetween(p1, gomel))
    }
}