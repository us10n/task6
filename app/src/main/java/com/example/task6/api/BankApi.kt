package com.example.task6.api

import com.example.task6.api.entity.AtmMachine
import com.example.task6.api.entity.BankFilial
import com.example.task6.api.entity.Infobox
import io.reactivex.rxjava3.core.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BankApi {

    private companion object {
        const val atmUrl = "atm"
        const val infoboxUrl = "infobox"
        const val filialUrl = "filials_info"
        const val cityValue = "city"
    }

    @GET(atmUrl)
    fun getAllATMMachines(@Query("city") cityName: String): Call<List<AtmMachine>>

    @GET(infoboxUrl)
    fun getAllInfoboxes(@Query(cityValue) cityName: String): Call<List<Infobox>>

    @GET(filialUrl)
    fun getAllBankFilials(@Query(cityValue) cityName: String): Call<List<BankFilial>>

}