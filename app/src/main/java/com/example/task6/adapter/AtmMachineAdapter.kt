package com.example.task6.adapter

import com.example.task6.api.entity.AtmMachine
import com.google.android.gms.maps.model.LatLng

class AtmMachineAdapter {
    companion object {
        val instance = AtmMachineAdapter()
    }

    fun atmMachineToLatLng(atmMachine: AtmMachine): LatLng =
        LatLng(atmMachine.gps_x.toDouble(), atmMachine.gps_y.toDouble())
}