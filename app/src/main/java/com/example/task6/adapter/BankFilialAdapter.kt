package com.example.task6.adapter

import com.example.task6.api.entity.BankFilial
import com.google.android.gms.maps.model.LatLng

class BankFilialAdapter {
    companion object {
        val instance = BankFilialAdapter()
    }
    fun bankFilialToLatLng(bankFilial: BankFilial): LatLng =
        LatLng(bankFilial.GPS_X.toDouble(), bankFilial.GPS_Y.toDouble())
}