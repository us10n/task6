package com.example.task6.adapter

import com.example.task6.api.entity.Infobox
import com.google.android.gms.maps.model.LatLng

class InfoboxAdapter {
    companion object {
        val instance = InfoboxAdapter()
    }
    fun infoboxToLatLng(infobox: Infobox): LatLng =
        LatLng(infobox.gps_x.toDouble(), infobox.gps_y.toDouble())
}