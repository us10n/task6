package com.example.task6.di

import com.example.task6.api.BankApi
import dagger.Module
import dagger.Provides
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    private companion object {
        const val retrofitBaseUrl = "https://belarusbank.by/api/"
    }

    @Singleton
    @Provides
    fun provideRetrofitApi(): BankApi = Retrofit.Builder()
        .baseUrl(retrofitBaseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .build()
        .create(BankApi::class.java)

}