package com.example.task6.di

import android.app.Application
import com.example.task6.MapsActivity
import com.example.task6.viewmodel.ViewModelFactory
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {
    fun viewModelsFactory(): ViewModelFactory
    fun inject(mapsActivity: MapsActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}