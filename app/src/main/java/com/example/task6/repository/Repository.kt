package com.example.task6.repository

import com.example.task6.api.BankApi
import com.example.task6.api.entity.AtmMachine
import com.example.task6.api.entity.BankFilial
import com.example.task6.api.entity.Infobox
import javax.inject.Inject

class Repository @Inject constructor(
    private val bankApi: BankApi
) {
    fun getAllAtms(cityName: String) = bankApi.getAllATMMachines(cityName)
    fun getAllInfoboxes(cityName: String) = bankApi.getAllInfoboxes(cityName)
    fun getAllBankFilials(cityName: String)= bankApi.getAllBankFilials(cityName)
}